```
#!markdown


```
**Tema 3 - XML**
===================

Este documento las distintas interfaces de la aplicación y su comportamiento.

----------


Ejercicio 1 - Empleados
-------------

Leer una lista de empleados y a partir de los datos obtenidos sacar la edad media, el empleado con el salario máximo y con el mínimo.

![ex1.png](https://lh3.googleusercontent.com/-s4yr6QEJ6ko/WE2_oXm3K1I/AAAAAAAACcI/iSBBjzQB5tQANFg1LWWT2Kdbsoz739OUwCLcB/s0/ex1.png)

Ejercicio 2 - El tiempo
-------------

Obtener xml desde **aemet** y conseguir predicción del tiempo para hoy y para el día siguiente.

![ex2.png](https://lh3.googleusercontent.com/-4BtJisy8Mng/WE3A5XkG9EI/AAAAAAAACcQ/eDYZ0KmN-Gcimc35QiWACq-YOcrwBCJGgCLcB/s0/ex2.png)

> **Nota:**
>  Las celdas de los periodos horarios se crean dinámicamente dependiendo si contienen información o no.
>  Lo único es que lo muestra de forma desordenada por una extraña razón. La información de los tramos horarios es un hashmap.

Ejercicio 3 - Bizi
-------------

Conseguir listado de estaciones de bici de zaragoza y mostrar este listado en pantalla.

![ex3_1.png](https://lh3.googleusercontent.com/-6DprnyFeKz4/WE3B8EMkABI/AAAAAAAACck/mMB6oRkl42Y8dpfZjl2ZfCyb8WJann6iACLcB/s0/ex3_1.png)

Información sobre la estación elegida.

![ex4_2.png](https://bitbucket.org/repo/G57aML/images/2513762335-ex4_2.png)

Ejercicio 4 - Noticias
-------------

Descargar los feeds de los portales especificados en la primera imagen y mostrar el listado de las noticias de cada uno. Al pulsar en una de ellas se abre el navegador con la noticia seleccionada.


![ex4_1.png](https://lh3.googleusercontent.com/-xo_95NIk648/WE3Da-0GkhI/AAAAAAAACc4/y7aLBH3ZSXUpsdC8hvskaDNsgOQMXiE6QCLcB/s0/ex4_1.png)
![ex4_2.png](https://lh3.googleusercontent.com/-nW1lwtx9sC8/WE3Dgqsg9FI/AAAAAAAACdA/-qm3FtelK547aSaHkF870pE63DLjjsbwgCLcB/s0/ex4_2.png)