package es.dpinfo.acdata_tema3;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.XmlResourceParser;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import es.dpinfo.acdata_tema3.Exercise1.models.Employer;
import es.dpinfo.acdata_tema3.Exercise2.models.MeteoTime;
import es.dpinfo.acdata_tema3.Exercise3.models.Station;
import es.dpinfo.acdata_tema3.Exercise4.models.News;

/**
 * Created by dprimenko on 5/12/16.
 */
public class XMLUtilities {

    public static ArrayList<Employer> readEmployersXml(XmlResourceParser file) {
        XmlPullParser xpp = file;
        ArrayList<Employer> empleados = null;
        int eventType = 0;
        try {
            eventType = xpp.getEventType();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        Employer empleadoAux = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            try {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        empleados = new ArrayList();
                        break;
                    case XmlPullParser.START_TAG:
                        name = xpp.getName();
                        if (name.equals("empleado")) {
                            empleadoAux = new Employer();
                        } else if (empleadoAux != null) {
                            if (name.equals("name")) {
                                empleadoAux.setName(xpp.nextText());
                            } else if (name.equals("rol")) {
                                empleadoAux.setRol(xpp.nextText());
                            } else if (name.equals("age")) {
                                empleadoAux.setAge(Integer.parseInt(xpp.nextText()));
                            } else if (name.equals("salary")) {
                                empleadoAux.setSalary(Float.parseFloat(xpp.nextText()));
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = xpp.getName();
                        if (name.equalsIgnoreCase("empleado") && empleadoAux != null) {
                            empleados.add(empleadoAux);
                        }
                }
                eventType = xpp.next();
            }
            catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return empleados;
    }

    public interface Downloaded {
        void onDownloaded();
    }

    public static ArrayList<News> readFeedXml(String fileXml) {

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

        XmlPullParser xpp = null;
        XmlPullParserFactory factory = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            xpp = factory.newPullParser();
            xpp.setInput( new StringReader(fileXml) );
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        ArrayList<News> news = null;
        int eventType = 0;
        try {
            eventType = xpp.getEventType();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        News newsAux = new News();

        while (eventType != XmlPullParser.END_DOCUMENT){
            try {
                String tagName = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        news = new ArrayList();
                        break;
                    case XmlPullParser.START_TAG:
                        tagName = xpp.getName();
                        if (tagName.equals("item")) {
                            newsAux = new News();
                        } else if (tagName.equals("title")) {
                            newsAux.setTitle(xpp.nextText());
                        } else if (tagName.equals("description")) {
                            newsAux.setDesc(xpp.nextText());
                        } else if (tagName.equals("pubDate")) {
                            newsAux.setPubDate(xpp.nextText());
                        } else if (tagName.equals("link")) {
                            newsAux.setLink(xpp.nextText());
                        }

                        break;
                    case XmlPullParser.END_TAG:
                        tagName = xpp.getName();
                        if (tagName.equalsIgnoreCase("item")) {
                            news.add(newsAux);
                        }
                }
                eventType = xpp.next();
            }
            catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        newsAux.setPubDate(newsAux.getPubDate());
        return news;
    }

    public static ArrayList<MeteoTime> readMeteoXml(String fileXml) {

        // Fechas de hoy y mañana
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = df.format(Calendar.getInstance().getTime());
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        dt = c.getTime();
        String nextDay = df.format(dt);

        XmlPullParser xpp = null;
        XmlPullParserFactory factory = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            xpp = factory.newPullParser();
            xpp.setInput( new StringReader(fileXml) );
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        ArrayList<MeteoTime> time = null;
        int eventType = 0;
        int tempEvent = 0;
        boolean diaValido = false;

        try {
            eventType = xpp.getEventType();
            tempEvent = xpp.getEventType();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        MeteoTime day = null;
        HashMap<String, String> schedule = null;
        boolean temperatura = false;
        schedule = new HashMap<>();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            try {
                String tagName = null;

                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        time = new ArrayList<MeteoTime>();
                        break;
                    case XmlPullParser.START_TAG:
                        tagName = xpp.getName();


                        if ((tagName.equals("dia"))) {
                            if (xpp.getAttributeName(0).equals("fecha")) {
                                day = new MeteoTime();

                                String date = xpp.getAttributeValue(0);

                                if (date.equals(currentDate) || date.equals(nextDay)) {
                                    day.setDate(date);
                                    diaValido = true;
                                }
                                else {
                                    diaValido = false;
                                }
                            }
                        }
                        else if (diaValido) {
                            if (tagName.equals("estado_cielo")) {
                                if (xpp.getAttributeName(1).equals("descripcion")) {
                                    String desc = xpp.getAttributeValue(1);
                                    if (!(desc.equals(""))) {
                                        if (xpp.getAttributeName(0).equals("periodo")) {
                                            String period = xpp.getAttributeValue(0);

                                            if (period.equals("00-06")) {
                                                schedule.put(period, xpp.nextText());
                                            }else if (period.equals("06-12")) {
                                                schedule.put(period, xpp.nextText());
                                            } else if (period.equals("12-18")) {
                                                schedule.put(period, xpp.nextText());
                                            } else if (period.equals("18-24")) {
                                                schedule.put(period, xpp.nextText());
                                            }
                                        }
                                    }
                                }
                            }
                            else if (tagName.equals("temperatura")) {

                                temperatura = true;
                                tagName = xpp.getName();

                            }else if (tagName.equals("maxima") && temperatura) {
                                day.setMax(xpp.nextText());
                            } else if(tagName.equals("minima") && temperatura) {
                                day.setMin(xpp.nextText());
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        tagName = xpp.getName();
                        if (tagName.equalsIgnoreCase("dia") && diaValido) {
                            day.setSchedule(schedule);
                            time.add(day);
                            schedule = new HashMap<>();
                        }

                        else if(tagName.equalsIgnoreCase("temperatura") && diaValido) {
                            temperatura = false;
                        }
                        break;
                }

                eventType = xpp.next();

            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return time;
    }

    public static ArrayList<Station> readStations(String fileXml) throws IOException, XmlPullParserException {
        XmlPullParser xpp = null;
        XmlPullParserFactory factory = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            xpp = factory.newPullParser();
            xpp.setInput( new StringReader(fileXml) );
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        ArrayList<Station> stations = null;
        int eventType = 0;
        eventType = xpp.getEventType();
        Station station = null;
        boolean inEstacion = false;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser. START_DOCUMENT :
                    stations = new ArrayList<>();
                    break;
                case XmlPullParser. START_TAG :
                    if (xpp.getName().equalsIgnoreCase("estacion")) {
                        inEstacion = true;
                        station = new Station();
                    }
                    if (inEstacion && xpp.getName().equalsIgnoreCase("uri")) {
                        station.setUrl(xpp.nextText());
                    }
                    if (inEstacion && xpp.getName().equalsIgnoreCase("title")) {
                        station.setTitle(xpp.nextText());
                    }
                    if (inEstacion && xpp.getName().equalsIgnoreCase("estado")) {
                        station.setState(xpp.nextText());
                    }
                    if (inEstacion && xpp.getName().equalsIgnoreCase("bicisDisponibles")) {
                        station.setAvailable(xpp.nextText());
                    }
                    break;
                case XmlPullParser. END_TAG :
                    if (xpp.getName().equalsIgnoreCase("estacion")) {
                        inEstacion = false;
                        try {
                            stations.add(station);
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    break;
            }
            eventType = xpp.next();
        }

        return stations;
    }

    public static void downloadFileXml(String xml, String temporal, final Context context, final Downloaded downloaded) {
        final ProgressDialog progreso = new ProgressDialog(context);
        File myFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), temporal);
        RestClient.get(xml, new FileAsyncHttpResponseHandler(myFile) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                downloaded.onDownloaded();
                Toast.makeText(context, "XML: " + file.getPath(), Toast.LENGTH_SHORT).show();
                progreso.dismiss();
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
        });
    }
}
