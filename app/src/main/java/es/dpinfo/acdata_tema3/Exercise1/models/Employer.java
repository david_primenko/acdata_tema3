package es.dpinfo.acdata_tema3.Exercise1.models;

/**
 * Created by usuario on 5/12/16.
 */

public class Employer {

    private String name;
    private String rol;
    private int age;
    private double salary;

    public String getName() {
        return name;
    }
    public String getRol() {
        return rol;
    }
    public int getAge() {
        return age;
    }
    public double getSalary() {
        return salary;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setRol(String rol) {
        this.rol = rol;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setSalary(double salary) {
        this.salary = salary;
    }

    /*public Employer(String name, String rol, int age, double salary) {
        this.name = name;
        this.rol = rol;
        this.age = age;
        this.salary = salary;
    }*/
}
