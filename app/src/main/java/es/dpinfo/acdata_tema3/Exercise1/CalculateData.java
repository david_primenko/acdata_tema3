package es.dpinfo.acdata_tema3.Exercise1;

import java.util.ArrayList;

import es.dpinfo.acdata_tema3.Exercise1.models.Employer;

/**
 * Created by usuario on 5/12/16.
 */

public class CalculateData {

    private ArrayList<Employer> employers;

    public CalculateData(ArrayList<Employer> employers) {
        this.employers = employers;
    }

    public int getAverageAge() {
        int total = 0;
        int result = 0;

        for (Employer employer:employers) {
            total += employer.getAge();
        }
        result = (total / 2);
        return result;
    }

    public Employer getMinimumSalary() {
        Employer result = null;
        for (int i = 0; i < employers.size(); i++) {
            if (i == 0) {
                result = employers.get(i);
            }

            if (result.getAge() > employers.get(i).getAge()) {
                result = employers.get(i);
            }
        }
        return result;
    }

    public Employer getMaximumSalary() {
        //Employer result = new Employer("","", 0, 0);
        Employer result = new Employer();

        for (Employer employer:employers) {
            if (result.getAge() < employer.getAge()) {
                result = employer;
            }
        }

        return result;
    }
}
