package es.dpinfo.acdata_tema3.Exercise4.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import es.dpinfo.acdata_tema3.R;
import es.dpinfo.acdata_tema3.XMLUtilities;

/**
 * Created by dprimenko on 7/12/16.
 */
public class IndexFeedFragment extends Fragment implements View.OnClickListener{

    public static final String FRAGMENT_TAG = "fragmenta";
    private FragmentIterationListener mCallback;

    private static final String RSS_PAIS = "http://ep00.epimg.net/rss/elpais/portada.xml";
    private static final String TEMP_PAIS = "elpais.xml";

    private static final String RSS_MUNDO = "http://estaticos.elmundo.es/elmundo/rss/portada.xml";
    private static final String TEMP_MUNDO = "elmundo.xml";

    private static final String RSS_LINUX = "http://www.linux-magazine.com/rss/feed/lmi_news";
    private static final String TEMP_LINUX = "lm.xml";

    private static final String RSS_PCWORLD = "http://www.pcworld.com/index.rss";
    private static final String TEMP_PCWORLD = "pcworld.xml";

    private ImageView imvMundo, imvPais, imvLinux, imvPcWorld;



    public interface FragmentIterationListener{
        void onFragmentIterationListener(String fileXml);
    }

    public static IndexFeedFragment newInstance(Bundle arguments){
        IndexFeedFragment indexFragment = new IndexFeedFragment();

        if(arguments != null){
            indexFragment.setArguments(arguments);
        }

        return indexFragment;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);

        try {
            mCallback = (FragmentIterationListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement FragmentIterationListener");
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            mCallback = (FragmentIterationListener)context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString()+" must implement FragmentIterationListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_index_feed, container, false);

        if(rootView != null){

            imvMundo = (ImageView) rootView.findViewById(R.id.imv_mundo);
            imvPais = (ImageView) rootView.findViewById(R.id.imv_pais);
            imvLinux = (ImageView) rootView.findViewById(R.id.imv_linux);
            imvPcWorld = (ImageView) rootView.findViewById(R.id.imv_pcworld);

            imvMundo.setOnClickListener(this);
            imvPais.setOnClickListener(this);
            imvLinux.setOnClickListener(this);
            imvPcWorld.setOnClickListener(this);
        }

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public void onClick(View v) {

        XMLUtilities.Downloaded downloaded;
        int view = v.getId();

        switch (view) {
            case R.id.imv_mundo:
                downloaded = new XMLUtilities.Downloaded() {
                    @Override
                    public void onDownloaded() {
                        mCallback.onFragmentIterationListener(TEMP_MUNDO);
                    }
                };
                XMLUtilities.downloadFileXml(RSS_MUNDO, TEMP_MUNDO, getActivity(), downloaded);
                break;
            case R.id.imv_pais:
                downloaded = new XMLUtilities.Downloaded() {
                    @Override
                    public void onDownloaded() {
                        mCallback.onFragmentIterationListener(TEMP_PAIS);
                    }
                };
                XMLUtilities.downloadFileXml(RSS_PAIS, TEMP_PAIS, getActivity(), downloaded);
                break;
            case R.id.imv_linux:
                downloaded = new XMLUtilities.Downloaded() {
                    @Override
                    public void onDownloaded() {
                        mCallback.onFragmentIterationListener(TEMP_LINUX);
                    }
                };
                XMLUtilities.downloadFileXml(RSS_LINUX, TEMP_LINUX, getActivity(), downloaded);
                break;
            case R.id.imv_pcworld:
                downloaded = new XMLUtilities.Downloaded() {
                    @Override
                    public void onDownloaded() {
                        mCallback.onFragmentIterationListener(TEMP_PCWORLD);
                    }
                };
                XMLUtilities.downloadFileXml(RSS_PCWORLD, TEMP_PCWORLD, getActivity(), downloaded);
                break;
        }
    }



}
