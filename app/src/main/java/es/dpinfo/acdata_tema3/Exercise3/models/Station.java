package es.dpinfo.acdata_tema3.Exercise3.models;

/**
 * Created by dprimenko on 9/12/16.
 */
public class Station {

    private String title;
    private String state;
    private String available;
    private String url;

    public Station(){

    }
    public Station(String titulo, String estado, String bicisD, String url) {
        this.title = titulo;
        this.state = estado;
        this.available = bicisD;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
