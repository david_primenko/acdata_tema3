package es.dpinfo.acdata_tema3.Exercise3;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import es.dpinfo.acdata_tema3.Exercise3.models.Station;
import es.dpinfo.acdata_tema3.R;

/**
 * Created by dprimenko on 9/12/16.
 */
public class StationFragment extends DialogFragment{

    private Station station;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ProductHolder p = new ProductHolder();
        View rootView = inflater.inflate(R.layout.fragment_station, container, false);
        p.station = (TextView) rootView.findViewById(R.id.txv_station);
        p.state = (TextView) rootView.findViewById(R.id.txv_state);
        p.nBikes = (TextView) rootView.findViewById(R.id.txv_available);

        p.station.setText(station.getTitle());
        p.state.setText(station.getState());
        p.nBikes.setText(station.getAvailable());
        rootView.setTag(p);
        return rootView;
    }

    public void setStation (Station station){
        this.station = station;
    }


    class ProductHolder{
        TextView station, state, nBikes;
    }
}
