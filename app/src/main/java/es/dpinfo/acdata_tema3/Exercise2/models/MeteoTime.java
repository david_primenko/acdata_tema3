package es.dpinfo.acdata_tema3.Exercise2.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dprimenko on 8/12/16.
 */
public class MeteoTime {

    private String date;
    private HashMap<String, String> schedule;
    private String max;
    private String min;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public HashMap<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(HashMap<String, String> schedule) {
        this.schedule = schedule;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }
}
