package es.dpinfo.acdata_tema3.Exercise2;

import java.util.ArrayList;

import es.dpinfo.acdata_tema3.Exercise2.models.MeteoTime;

/**
 * Created by dprimenko on 9/12/16.
 */
public class TimeSingleton {

    private ArrayList<MeteoTime> time;

    public ArrayList<MeteoTime> getTime() {
        return time;
    }

    public void setTime(ArrayList<MeteoTime> time) {
        this.time = time;
    }

    private static TimeSingleton ourInstance = new TimeSingleton();

    public static TimeSingleton getInstance() {
        return ourInstance;
    }

    private TimeSingleton() {
    }

    public MeteoTime getToday() {
        return time.get(0);
    }

    public MeteoTime getTomorrow() {
        return time.get(1);
    }
}
