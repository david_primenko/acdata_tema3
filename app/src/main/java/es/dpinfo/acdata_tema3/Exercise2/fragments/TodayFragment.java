package es.dpinfo.acdata_tema3.Exercise2.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Map;

import es.dpinfo.acdata_tema3.Exercise2.TimeSingleton;
import es.dpinfo.acdata_tema3.Exercise2.models.MeteoTime;
import es.dpinfo.acdata_tema3.R;

/**
 * Created by dprimenko on 6/12/16.
 */
public class TodayFragment extends Fragment{

    private TextView txvDate, txvTempMin, txvTempMax;
    private LinearLayout ll_hours;
    private MeteoTime today;
    private static final String IMG_START = "http://www.aemet.es/imagenes/png/estado_cielo/";
    private static final String IMG_END = "_g.png";

    public TodayFragment() {
        today = TimeSingleton.getInstance().getToday();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_today, container, false);

        if (rootView != null) {
            txvDate = (TextView) rootView.findViewById(R.id.txv_date_today);
            txvTempMin = (TextView) rootView.findViewById(R.id.txv_temp_min_today);
            txvTempMax = (TextView) rootView.findViewById(R.id.txv_temp_max_today);
            ll_hours = (LinearLayout) rootView.findViewById(R.id.ll_hours_today);

            txvDate.setText(today.getDate());
            txvTempMin.setText(today.getMin());
            txvTempMax.setText(today.getMax());

            addSchedule();
        }

        return rootView;
    }

    private void addSchedule() {
        for (Map.Entry<String, String> entry: today.getSchedule().entrySet()) {
            ll_hours.addView(inflateView(entry.getKey(), entry.getValue()));
        }
    }

    private View inflateView(String period, String img) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.item_schedule, null);
        LinearLayout llSchedule;
        LinearLayout.LayoutParams llScheduleParams;
        TextView txvSchedule, txvTemp;
        ImageView imvScheduleImg;

        llScheduleParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);

        if (view != null) {
            txvSchedule = (TextView) view.findViewById(R.id.txv_schedule);
            llSchedule = (LinearLayout) view.findViewById(R.id.ll_schedule);
            imvScheduleImg = (ImageView) view.findViewById(R.id.imv_schedule_img);
            llSchedule.setLayoutParams(llScheduleParams);

            Picasso.with(getContext())
                    .load(IMG_START + img + IMG_END)
                    .into(
                            imvScheduleImg
                    );

            txvSchedule.setText(period);
        }

        return view;
    }
}
