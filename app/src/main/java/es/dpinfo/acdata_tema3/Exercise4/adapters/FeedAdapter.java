package es.dpinfo.acdata_tema3.Exercise4.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import es.dpinfo.acdata_tema3.Exercise4.models.News;
import es.dpinfo.acdata_tema3.R;

/**
 * Created by dprimenko on 7/12/16.
 */
public class FeedAdapter extends ArrayAdapter<News>{

    Context context;

    public FeedAdapter(Context context, List<News> objects) {
        super(context, R.layout.item_news, objects);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Holder holder;

        if (view == null) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_news, null);

            holder = new Holder();

            holder.txvTitle = (TextView) view.findViewById(R.id.txv_item_title_news);
            holder.txvDate = (TextView) view.findViewById(R.id.txv_item_date_news);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }

        holder.txvTitle.setText(getItem(position).getTitle());
        holder.txvDate.setText(getItem(position).getPubDate());

        return view;
    }

    class Holder{
        TextView txvTitle;
        TextView txvDate;
    }
}
