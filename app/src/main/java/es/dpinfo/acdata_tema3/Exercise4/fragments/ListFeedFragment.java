package es.dpinfo.acdata_tema3.Exercise4.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import es.dpinfo.acdata_tema3.Exercise4.adapters.FeedAdapter;
import es.dpinfo.acdata_tema3.Exercise4.models.News;
import es.dpinfo.acdata_tema3.IOUtilities;
import es.dpinfo.acdata_tema3.R;
import es.dpinfo.acdata_tema3.XMLUtilities;

/**
 * Created by dprimenko on 7/12/16.
 */
public class ListFeedFragment extends Fragment implements ListView.OnItemClickListener{

    public static final String FRAGMENT_TAG = "fragmentbs";

    private String temporal;

    private ListView lwFeed;
    private FeedAdapter adapter;

    private ArrayList<News> news;


    public static ListFeedFragment newInstance(Bundle arguments) {
        ListFeedFragment listFeedFragment = new ListFeedFragment();

        if (arguments != null) {
            listFeedFragment.setArguments(arguments);
        }

        return listFeedFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_list_feed, container, false);

        if (rootView != null) {
            news = XMLUtilities.readFeedXml(IOUtilities.readExternal(getArguments().getString("file_xml"), "UTF-8"));
            adapter = new FeedAdapter(getActivity(), news);
            lwFeed = (ListView) rootView.findViewById(R.id.lw_feed);
            lwFeed.setAdapter(adapter);
            lwFeed.setOnItemClickListener(this);
        }

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Uri uri = Uri.parse(news.get(position).getLink());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
