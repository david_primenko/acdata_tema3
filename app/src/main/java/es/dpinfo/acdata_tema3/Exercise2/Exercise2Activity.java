package es.dpinfo.acdata_tema3.Exercise2;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.dpinfo.acdata_tema3.Exercise2.fragments.TodayFragment;
import es.dpinfo.acdata_tema3.Exercise2.fragments.TomorrowFragment;
import es.dpinfo.acdata_tema3.Exercise2.models.MeteoTime;
import es.dpinfo.acdata_tema3.IOUtilities;
import es.dpinfo.acdata_tema3.R;
import es.dpinfo.acdata_tema3.XMLUtilities;

/**
 * Created by usuario on 5/12/16.
 */

public class Exercise2Activity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public static final String URL_XML = "http://www.aemet.es/xml/municipios/localidad_29067.xml";
    public static final String TEMPORAL = "meteo.xml";
    private XMLUtilities.Downloaded downloaded;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_2);

        tabLayout = (TabLayout) findViewById(R.id.tabs_exercise_2);
        viewPager = (ViewPager) findViewById(R.id.vp_exercise_2);

        loadInfo();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragment(new TodayFragment(), "Hoy");
        viewPagerAdapter.addFragment(new TomorrowFragment(), "Mañana");
        viewPager.setAdapter(viewPagerAdapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void loadInfo() {

        downloaded = new XMLUtilities.Downloaded() {
            @Override
            public void onDownloaded() {
                TimeSingleton.getInstance().setTime(XMLUtilities.readMeteoXml(IOUtilities.readExternal(Exercise2Activity.TEMPORAL, "UTF-8")));
                setupViewPager(viewPager);
                tabLayout.setupWithViewPager(viewPager);
            }
        };

        XMLUtilities.downloadFileXml(Exercise2Activity.URL_XML, Exercise2Activity.TEMPORAL, this, downloaded);
    }
}
