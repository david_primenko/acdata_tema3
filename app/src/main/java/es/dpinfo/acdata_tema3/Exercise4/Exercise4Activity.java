package es.dpinfo.acdata_tema3.Exercise4;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import es.dpinfo.acdata_tema3.Exercise4.fragments.IndexFeedFragment;
import es.dpinfo.acdata_tema3.Exercise4.fragments.ListFeedFragment;
import es.dpinfo.acdata_tema3.R;

/**
 * Created by dprimenko on 7/12/16.
 */
public class Exercise4Activity extends AppCompatActivity implements IndexFeedFragment.FragmentIterationListener{

    private IndexFeedFragment indexFeedFragment;
    private ListFeedFragment listFeedFragment;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_4);

        indexFeedFragment = (IndexFeedFragment) getFragmentManager().findFragmentByTag(IndexFeedFragment.FRAGMENT_TAG);

        if (indexFeedFragment == null) {
            indexFeedFragment = new IndexFeedFragment();
            getFragmentManager().beginTransaction().add(R.id.rl_exercise_4, indexFeedFragment, IndexFeedFragment.FRAGMENT_TAG).commit();
        }
    }

    @Override
    public void onFragmentIterationListener(String fileXml) {
        Bundle bundle = new Bundle();

        bundle.putString("file_xml", fileXml);

        listFeedFragment = ListFeedFragment.newInstance(bundle);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.rl_exercise_4, listFeedFragment, ListFeedFragment.FRAGMENT_TAG);
        transaction.addToBackStack(null);
        transaction.commit();

    }

}
