package es.dpinfo.acdata_tema3;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import es.dpinfo.acdata_tema3.Exercise1.Exercise1Activity;
import es.dpinfo.acdata_tema3.Exercise2.Exercise2Activity;
import es.dpinfo.acdata_tema3.Exercise3.Exercise3Activity;
import es.dpinfo.acdata_tema3.Exercise4.Exercise4Activity;
import es.dpinfo.acdata_tema3.Exercise4.fragments.IndexFeedFragment;
import es.dpinfo.acdata_tema3.Exercise4.fragments.ListFeedFragment;
import es.dpinfo.acdata_tema3.Exercise4.models.News;

/**
 * Created by usuario on 5/12/16.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnEx1, btnEx2, btnEx3, btnEx4;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnEx1 = (Button) findViewById(R.id.btn_ex_1);
        btnEx2 = (Button) findViewById(R.id.btn_ex_2);
        btnEx3 = (Button) findViewById(R.id.btn_ex_3);
        btnEx4 = (Button) findViewById(R.id.btn_ex_4);

        btnEx1.setOnClickListener(this);
        btnEx2.setOnClickListener(this);
        btnEx3.setOnClickListener(this);
        btnEx4.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        Intent intent = null;

        switch (id) {
            case R.id.btn_ex_1:
                intent = new Intent(MainActivity.this, Exercise1Activity.class);
                startActivity(intent);
                break;
            case R.id.btn_ex_2:
                intent = new Intent(MainActivity.this, Exercise2Activity.class);
                startActivity(intent);
                break;
            case R.id.btn_ex_3:
                intent = new Intent(MainActivity.this, Exercise3Activity.class);
                startActivity(intent);
                break;
            case R.id.btn_ex_4:
                intent = new Intent(MainActivity.this, Exercise4Activity.class);
                startActivity(intent);
                break;
        }
    }
}
