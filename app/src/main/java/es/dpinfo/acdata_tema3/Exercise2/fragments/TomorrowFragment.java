package es.dpinfo.acdata_tema3.Exercise2.fragments;

import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import es.dpinfo.acdata_tema3.Exercise2.Exercise2Activity;
import es.dpinfo.acdata_tema3.Exercise2.TimeSingleton;
import es.dpinfo.acdata_tema3.Exercise2.models.MeteoTime;
import es.dpinfo.acdata_tema3.IOUtilities;
import es.dpinfo.acdata_tema3.R;
import es.dpinfo.acdata_tema3.XMLUtilities;

/**
 * Created by dprimenko on 6/12/16.
 */
public class TomorrowFragment extends Fragment {

    private TextView txvDate, txvTempMin, txvTempMax;
    private LinearLayout ll_hours;
    private MeteoTime tomorrow;
    private static final String IMG_START = "http://www.aemet.es/imagenes/png/estado_cielo/";
    private static final String IMG_END = "_g.png";
    public TomorrowFragment() {
        tomorrow = TimeSingleton.getInstance().getTomorrow();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_tomorrow, container, false);

        if (rootView != null) {
            txvDate = (TextView) rootView.findViewById(R.id.txv_date_tomorrow);
            txvTempMin = (TextView) rootView.findViewById(R.id.txv_temp_min_tomorrow);
            txvTempMax = (TextView) rootView.findViewById(R.id.txv_temp_max_tomorrow);
            ll_hours = (LinearLayout) rootView.findViewById(R.id.ll_hours_tomorrow);

            txvDate.setText(tomorrow.getDate());
            txvTempMin.setText(tomorrow.getMin());
            txvTempMax.setText(tomorrow.getMax());

            addSchedule();
        }

        return rootView;
    }

    private void addSchedule() {
        for (Map.Entry<String, String> entry: tomorrow.getSchedule().entrySet()) {
            ll_hours.addView(inflateView(entry.getKey(), entry.getValue()));
        }
    }

    private View inflateView(String period, String img) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.item_schedule, null);
        LinearLayout llSchedule;
        LinearLayout.LayoutParams llScheduleParams;
        TextView txvSchedule, txvTemp;
        ImageView imvScheduleImg;

        llScheduleParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);

        if (view != null) {
            txvSchedule = (TextView) view.findViewById(R.id.txv_schedule);
            llSchedule = (LinearLayout) view.findViewById(R.id.ll_schedule);
            imvScheduleImg = (ImageView) view.findViewById(R.id.imv_schedule_img);
            llSchedule.setLayoutParams(llScheduleParams);

            Picasso.with(getContext())
                    .load(IMG_START + img + IMG_END)
                    .into(
                            imvScheduleImg
                    );

            txvSchedule.setText(period);
        }

        return view;
    }
}
