package es.dpinfo.acdata_tema3.Exercise3;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import es.dpinfo.acdata_tema3.Exercise3.models.Station;
import es.dpinfo.acdata_tema3.IOUtilities;
import es.dpinfo.acdata_tema3.R;
import es.dpinfo.acdata_tema3.XMLUtilities;

/**
 * Created by usuario on 5/12/16.
 */

public class Exercise3Activity extends AppCompatActivity {

    public static final String URL = "http://www.zaragoza.es/api/recurso/urbanismo-infraestructuras/estacion-bicicleta.xml";
    public static final String TEMPORAL = "bicis.xml";

    private ListView lwStations;
    private ArrayList<Station> stationsList;
    private ArrayList<String> stationsTitleList;
    private ArrayAdapter adapter;
    private XMLUtilities.Downloaded downloaded;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_3);

        lwStations = (ListView) findViewById(R.id.lw_stations);
        stationsTitleList = new ArrayList<>();

        loadInfo();
    }


    private void loadInfo() {
        downloaded = new XMLUtilities.Downloaded() {
            @Override
            public void onDownloaded() {
                try {
                    stationsList = XMLUtilities.readStations(IOUtilities.readExternal(TEMPORAL, "UTF-8"));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }

                for (Station station : stationsList) {
                    stationsTitleList.add(station.getTitle());
                }

                adapter = new ArrayAdapter(Exercise3Activity.this, R.layout.support_simple_spinner_dropdown_item, stationsTitleList);

                lwStations.setAdapter(adapter);

                lwStations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        StationFragment fragment = new StationFragment();
                        fragment.setStation(stationsList.get(position));
                        fragment.show(getSupportFragmentManager(), " ");
                    }
                });

                lwStations.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Uri uri = Uri.parse(stationsList.get(i).getUrl());
                        Intent in = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(in);
                        return true;
                    }
                });
            }
        };

        XMLUtilities.downloadFileXml(URL, TEMPORAL, this, downloaded);
    }
}
