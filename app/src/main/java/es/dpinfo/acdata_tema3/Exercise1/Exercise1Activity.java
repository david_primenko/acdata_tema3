package es.dpinfo.acdata_tema3.Exercise1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import es.dpinfo.acdata_tema3.Exercise1.models.Employer;
import es.dpinfo.acdata_tema3.R;
import es.dpinfo.acdata_tema3.XMLUtilities;

/**
 * Created by usuario on 5/12/16.
 */

public class Exercise1Activity extends AppCompatActivity {

    private TextView txvValueEdad, txvNameSalarioMinimo, txvValueSalarioMinimo, txvNameSalarioMaximo, txvValueSalarioMaximo;
    private FloatingActionButton fabRefresh;
    private ArrayList<Employer> employers;
    private Employer bestPaid;
    private Employer worstPaid;
    private CalculateData calculateData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_1);

        txvValueEdad = (TextView) findViewById(R.id.txv_value_edad);
        txvNameSalarioMaximo = (TextView) findViewById(R.id.txv_name_salario_minimo);
        txvNameSalarioMinimo = (TextView) findViewById(R.id.txv_name_salario_maximo);
        txvValueSalarioMinimo = (TextView) findViewById(R.id.txv_value_salario_minimo);
        txvValueSalarioMaximo = (TextView) findViewById(R.id.txv_value_salario_maximo);
        fabRefresh = (FloatingActionButton) findViewById(R.id.fab_refresh);


        fabRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loadXml()) {
                    loadData();
                }
            }
        });
    }

    private boolean loadXml() {

        boolean result = false;

        if ((employers = XMLUtilities.readEmployersXml(getResources().getXml(R.xml.empleados))) != null) {
            result = true;
        }

        return result;
    }

    private void loadData() {

        calculateData = new CalculateData(employers);

        bestPaid = calculateData.getMaximumSalary();
        worstPaid = calculateData.getMinimumSalary();

        txvValueEdad.setText(String.valueOf(calculateData.getAverageAge()));
        txvNameSalarioMinimo.setText(worstPaid.getName());
        txvValueSalarioMinimo.setText(String.valueOf(worstPaid.getSalary()));
        txvNameSalarioMaximo.setText(bestPaid.getName());
        txvValueSalarioMaximo.setText(String.valueOf(bestPaid.getSalary()));
    }
}
